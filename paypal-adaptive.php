<?php
    include 'constant.php';

    /**
    * Paypal Adaptive Payment
    * Author: Siddharth Pandey
    * Email: sid@mobiwebtech.com
    */

	$paypal_id = 'EHR764X7QW686';
	$shop_paypal_email = 'siddharthpandey21@gmail.com';
	//get parameters from the parameter.yml file
	$mode = paypal_mode;

	if ($mode == 'sandbox') {
	    $paypal_authorize_url = paypal_authorize_url_sandbox;
	    $paypal_email = paypal_email_sandbox;
	} else {
	    $paypal_authorize_url = paypal_authorize_url_live;
	    $paypal_email = paypal_email_live;
	}
	 
	$primary_user_amount  = 20;
	$secondry_user_amount = 5;
	$transaction_data = array(
        'primary_user_paypal_email' => $shop_paypal_email,
        'primary_user_amount' => $primary_user_amount,
        'secondry_user_paypal_email' => $paypal_email, 
        'secondry_user_amount' => $secondry_user_amount, 
        'paypal_id' => $paypal_id
    );

	$cancel_url = urlencode('http://localhost/paypal-adaptive/paypal-adaptive.php');
	$return_url = urlencode('http://localhost/paypal-adaptive/paypal-adaptive.php');

	$paypal_response = getPaypalResponse($transaction_data, $cancel_url, $return_url);
	
    echo '<pre>';
	print_r($paypal_response);
	echo '</pre>';

	if (isset($paypal_response->responseEnvelope)) {
	    if ($paypal_response->responseEnvelope->ack == 'Success') {
	        $pay_key = $paypal_response->payKey;
	        $reurn_data = array('link' => $paypal_authorize_url . $pay_key, 'cancel_url' => $cancel_url, 'return_url' => $return_url);
	        $data = array('code' => 101, 'message' => 'SUCCESS', 'data' => $reurn_data);
	    } else {
	        $data = array('code' => 1029, 'message' => 'FAILURE', 'data' => array());
	    }
	} else {
	    $data = array('code' => 1029, 'message' => 'FAILURE', 'data' => array());
	}

    /**
    * NOTES: you have to redirect into $data['data']['link'] to init payment, after that you have to get the transaction response in check.php, in check.php you have to pass the payKey.
    */
    echo '<pre>';
    print_r($data);
    echo '</pre>';

	returnResponse($data);

	function getPaypalResponse($transaction_data, $cancel_url, $return_url) {
        $card_percentage = 10;
        $pay_key = 'pay_key';
        $status = 'status';
        $id = 'id';
        $ipn_notification_url = '';
        $chained_payment_fee_payer = 'CHAINED_PAYMENT_FEE_PAYER';
        $ci_return_fee_payer = 'CI_RETURN_FEE_PAYER';
        $item_type_shop = 'SHOP';
        $miss_param = '';
        $result = array();

        //get parameters from the parameter.yml file
        $mode = paypal_mode;
        if ($mode == 'sandbox') {
            $paypal_acct_username = paypal_acct_username_sandbox;
            $paypal_acct_password = paypal_acct_password_sandbox;
            $paypal_acct_signature = paypal_acct_signature_sandbox;
            $paypal_acct_appid = paypal_acct_appid_sandbox;
            $paypal_end_point = paypal_end_point_sandbox;
            $paypal_acct_email_address = paypal_acct_email_address_sandbox;
            $paypal_acct_app_id = paypal_acct_appid_sandbox;
        } else {
            $paypal_acct_username = paypal_acct_username_live;
            $paypal_acct_password = paypal_acct_password_live;
            $paypal_acct_signature = paypal_acct_signature_live;
            $paypal_acct_appid = paypal_acct_appid_live;
            $paypal_end_point = paypal_end_point_live;
            $paypal_acct_email_address = paypal_acct_email_address_live;
            $paypal_acct_app_id = paypal_acct_appid_live;
        }
        $primary_reciever_paypal_email = $transaction_data['primary_user_paypal_email'];
        $primary_reciever_amount = $transaction_data['primary_user_amount'];
        $secondry_reciever_email = $transaction_data['secondry_user_paypal_email'];
        $secondry_reciever_amount = $transaction_data['secondry_user_amount'];
        $curreny_code = paypal_currency;
        //$paypal_transaction_service = $this->container->get('paypal_integration.paypal_transaction_check');
        //$paypal_service = $this->container->get('paypal_integration.payment_transaction');
        $type = $chained_payment_fee_payer;
        $item_type = $item_type_shop;
        $fee_payer = 'EACHRECEIVER';
        $feesPayerParam = '&feesPayer=';
        $final_fee_payer = $feesPayerParam.$fee_payer;
        $ipn_notification_url = urlencode(symfony_base_url.'ipn.php'); //ipn notification url
       
        $headers = array(
            'X-PAYPAL-SECURITY-USERID: ' . $paypal_acct_username,
            'X-PAYPAL-SECURITY-PASSWORD: ' . $paypal_acct_password,
            'X-PAYPAL-SECURITY-SIGNATURE: ' . $paypal_acct_signature,
            'X-PAYPAL-REQUEST-DATA-FORMAT: NV',
            'X-PAYPAL-RESPONSE-DATA-FORMAT: JSON',
            'X-PAYPAL-APPLICATION-ID: ' . $paypal_acct_appid,
        );
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        $payload = "actionType=PAY&ipnNotificationUrl=$ipn_notification_url&cancelUrl=$cancel_url&clientDetails.applicationId=$paypal_acct_app_id"
                . "&clientDetails.ipAddress=$ipAddress&currencyCode=$curreny_code" .
                "&receiverList.receiver(0).amount=$primary_reciever_amount&receiverList.receiver(0).email=$primary_reciever_paypal_email" .
                "&receiverList.receiver(0).primary=true&receiverList.receiver(1).amount=$secondry_reciever_amount" .
                "&receiverList.receiver(1).email=$secondry_reciever_email" .
                "&receiverList.receiver(1).primary=false" .
                "&requestEnvelope.errorLanguage=en_US" .
                "&returnUrl=$return_url".$final_fee_payer;
        $options = array(
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_POST => false,
            CURLOPT_POSTFIELDS => $payload,
            CURLOPT_RETURNTRANSFER => true
        );

        try {
            $curl = curl_init($paypal_end_point);
            if (!$curl) {
                throw new \Exception('Could not initialize curl');
            }
            if (!curl_setopt_array($curl, $options)) {
                throw new \Exception('Curl error:' . curl_error($curl));
            }
            $result = curl_exec($curl);
            if (!$result) {
                throw new \Exception('Curl error:' . curl_error($curl));
            }
            curl_close($curl);
            return json_decode($result);
        } catch (\Exception $e) {
            //$e->getMessage();
        }
    }

	function returnResponse($data_array) {
        echo json_encode($data_array, JSON_NUMERIC_CHECK);
        exit;
    }
?>