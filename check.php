<?php
    include 'constant.php';
    
    /**
    * Get transaction response
    * @param $transaction_pay_key
    * @return $rsult
    */
	function checkTransactionStatus($transaction_pay_key) {
        $result = array();
        $mode = paypal_mode;
        if ($mode == 'sandbox') {
            $paypal_acct_username = paypal_acct_username_sandbox;
            $paypal_acct_password = paypal_acct_password_sandbox;
            $paypal_acct_signature = paypal_acct_signature_sandbox;
            $paypal_acct_appid = paypal_acct_appid_sandbox;
            $paypal_end_point = paypal_detail_end_point_sandbox;
            $paypal_acct_email_address = paypal_acct_email_address_sandbox;
        } else {
            $paypal_acct_username = paypal_acct_username_live;
            $paypal_acct_password = paypal_acct_password_live;
            $paypal_acct_signature = paypal_acct_signature_live;
            $paypal_acct_appid = paypal_acct_appid_live;
            $paypal_end_point = paypal_detail_end_point_live;
            $paypal_acct_email_address = paypal_acct_email_address_live;
        }
        $headers = array(
            'X-PAYPAL-SECURITY-USERID: ' . $paypal_acct_username,
            'X-PAYPAL-SECURITY-PASSWORD: ' . $paypal_acct_password,
            'X-PAYPAL-SECURITY-SIGNATURE: ' . $paypal_acct_signature,
            'X-PAYPAL-REQUEST-DATA-FORMAT: NV',
            'X-PAYPAL-RESPONSE-DATA-FORMAT: JSON',
            'X-PAYPAL-APPLICATION-ID: ' . $paypal_acct_appid,
        );
        $payload = "payKey=" . $transaction_pay_key . "&requestEnvelope.errorLanguage=en_US";
        $options = array(
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_POST => false,
            CURLOPT_POSTFIELDS => $payload,
            CURLOPT_RETURNTRANSFER => true
        );
        
        try {
            $curl = curl_init($paypal_end_point);
            if (!$curl) {
                throw new \Exception('Could not initialize curl');
            }
            if (!curl_setopt_array($curl, $options)) {
                throw new \Exception('Curl error:' . curl_error($curl));
            }
            $result = curl_exec($curl);
            if (!$result) {
                throw new \Exception('Curl error:' . curl_error($curl));
            }
            curl_close($curl);
            return json_decode($result);
        } catch (\Exception $e) {
            //$e->getMessage();
        }
    }
    $data = checkTransactionStatus($payKey);
    echo '<pre>';
    print_r($data);
    echo '</pre>'; 
?>