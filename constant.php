<?php 
	/* Define Constant for paypal payment */
	define('paypal_mode', 'sandbox');
	define('paypal_authorize_url_sandbox', 'https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_ap-payment&paykey=');
	define('paypal_email_sandbox', '');

	define('paypal_authorize_url_live', '');
	define('paypal_email_live', '');

    define('paypal_acct_username_sandbox', '');
    define('paypal_acct_password_sandbox', '');
    define('paypal_acct_signature_sandbox', '');
    define('paypal_acct_appid_sandbox', 'APP-80W284485P519543T');
    define('paypal_end_point_sandbox', 'https://svcs.sandbox.paypal.com/AdaptivePayments/Pay');
    define('paypal_acct_email_address_sandbox', '');

    define('paypal_acct_username_live', '');
    define('paypal_acct_password_live', '');
    define('paypal_acct_signature_live', '');
    define('paypal_acct_appid_live', '');
    define('paypal_end_point_live', '');
    define('paypal_acct_email_address_live', '');

    define('paypal_currency','USD');
?>